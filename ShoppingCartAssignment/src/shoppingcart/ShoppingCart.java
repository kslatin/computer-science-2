package shoppingcart;

import java.util.ArrayList;

/**
 * 
 */

/**
 * @author Kyle
 *
 */
public class ShoppingCart {
	/**
	 * 
	 */
	public ShoppingCart() {
		super();
	}

	/**
	 * @param mylist
	 * @param thePerson
	 */
	public ShoppingCart(ArrayList<Item> mylist, Person thePerson) {
		super();
		this.mylist = mylist;
		this.thePerson = thePerson;
	}

	private ArrayList<Item> mylist = new ArrayList<Item>();
	private Person thePerson;

	/**
	 * 
	 * @return calculate total price
	 */
	public float calcTotal() {
		float total = 0;
		for (Item item : mylist) {
			total += item.getPrice();
		}
		return total;
	}

	/**
	 * @return the mylist
	 */
	public ArrayList<Item> getMylist() {
		return mylist;
	}

	/**
	 * @param mylist
	 *            the mylist to set
	 */
	public void setMylist(ArrayList<Item> mylist) {
		this.mylist = mylist;
	}

	/**
	 * @return the thePerson
	 */
	public Person getThePerson() {
		return thePerson;
	}

	/**
	 * @param thePerson
	 *            the thePerson to set
	 */
	public void setThePerson(Person thePerson) {
		this.thePerson = thePerson;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = "";
		result += "Shopping Cart \n";
		result += "-------------- \n";
		result += thePerson.toString();
		result += "-------------- \n";
		result += "Cart Details: \n";
		for (int i = 0; i < mylist.size(); i++) {
			result += "Item " + (i + 1);
			result += mylist.get(i).toString();
		}

		return result;
	}

	void payForItems(Payment p) {
		p.pay();
	}
}
