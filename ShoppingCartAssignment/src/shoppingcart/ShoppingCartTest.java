package shoppingcart;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Kyle
 *
 */
public class ShoppingCartTest {
	private static final int QUIT = 0;
	private static final int ADD = 1;
	private static final int SHOW = 2;
	private static final int TOTAL = 3;
	private static final int PAYCREDIT = 4;
	private static final int PAYPAL = 5;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		ShoppingCart mycart = new ShoppingCart();
		System.out.println("Welcome");
		System.out.println("What is your name?");
		String name = input.next();
		Person p = null;
		boolean doneWithPerson = false;
		while (doneWithPerson == false) {
			try {
				System.out.println("What is your age?");
				int age = input.nextInt();
				p = new Person(name, age);
				doneWithPerson = true;
			} catch (InputMismatchException e) {
				System.out.println("Please put in number!");
				input.next();
			}
		}

		mycart.setThePerson(p);

		/// The main method

		System.out.println(mycart);
		int choice = ADD;
		while (choice != QUIT) {
			// ask user to select a choice
			//
			System.out.println("Choose from the following options");
			System.out.println("0) Quit");
			System.out.println("1) Add items");
			System.out.println("2) Show cart");
			System.out.println("3) Get total");
			System.out.println("4) Pay by credit");
			System.out.println("5) Pay by paypal");
			choice = input.nextInt();

			switch (choice) {
			case QUIT:
				choice = QUIT;
				break;
			case ADD:
				break;
			case SHOW:
				System.out.println(mycart.toString());
				break;
			case TOTAL:
				System.out.println(mycart.calcTotal());
				break;
			case PAYCREDIT:
				mycart.payForItems(new CreditCard());
				break;
			case PAYPAL:
				mycart.payForItems(new PayPal());
				break;
			default:
				System.out.println("Invalid input");
				break;
			}

			// TODO: validate number input for price and id
			if (choice == ADD) {
				String more = "yes";
				ArrayList<Item> mylist = new ArrayList<Item>();
				while (more.toLowerCase().equals("yes")) {
					Item item = new Item();

					// ITEM NAME
					System.out.println("What is the item name?");
					String itemName = input.next();
					item.setName(itemName);

					// ITEM PRICE
					boolean doneWithPrice = false;
					while (doneWithPrice == false) {
						try {
							System.out.println("What is the item price?");
							float price = input.nextFloat();
							// If price is below zero, automatically set to zero
							if (price < 0) {
								price = 0;
							}

							// Set the price
							item.setPrice(price);

							doneWithPrice = true;
						} catch (InputMismatchException e) {
							System.out.println("Price must be numeric!");
							input.next();
						}
					}

					// ITEM ID
					boolean doneWithID = false;
					while (doneWithID == false) {
						try {
							System.out.println("What is the item id");
							int id = input.nextInt();
							item.setId(id);

							doneWithID = true;
						} catch (InputMismatchException e) {
							System.out.println("ID must be numeric!");
							input.next();
						}
					}

					mylist.add(item);
					boolean validContinue = false;
					while (!validContinue) {
						System.out.println("Do you want to continue yes or no? (yes/no)");
						more = input.next();
						if (more.toLowerCase().equals("yes") || more.toLowerCase().equals("no")) {
							break;
						} else {
							System.out.println("Please enter 'yes' or 'no'!");
						}
					}
				}
				mycart.setMylist(mylist);
				System.out.println(mycart);
			}
		}

		System.out.println("Thank you for shopping!");
	}

	public float calcTotal() {
		float total = 0;

		// for (Item i : mylist) {
		// total += i.getPrice();
		// }
		return total;
	}
}
