package shoppingcart;

public class Item {
	/**
	 * 
	 */
	public Item() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param price
	 */
	public Item(int id, String name, float price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}

	private int id;
	private String name;
	private float price;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String res = "";
		res += "\t ID: " + this.id + "\n";
		res += "\t Name: " + this.name + "\n";
		res += "\t Price: " + this.price + "\n";
		return res;
	}
}
