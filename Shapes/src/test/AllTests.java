package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CircleTest.class, ShapeTest.class, SquareTest.class, TriangleTest.class })
public class AllTests {

}
