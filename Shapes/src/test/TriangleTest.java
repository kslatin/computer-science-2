package test;

import org.junit.Assert;
import org.junit.Test;

import model.Triangle;

public class TriangleTest {

	@Test
	public void test() {
		Triangle t = new Triangle();
		t.setSideA(4.0);
		t.setSideB(5.0);
		t.setSideC(6.0);
		Assert.assertEquals(t.getSideA(), 4.0, 0.0);
		Assert.assertEquals(t.getSideB(), 5.0, 0.0);
		Assert.assertEquals(t.getSideC(), 6.0, 0.0);
		System.out.println(t.calcArea());
		Assert.assertEquals(t.calcArea(), 9.9, 0.03);
	}

}
