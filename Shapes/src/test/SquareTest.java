package test;

import org.junit.Assert;
import org.junit.Test;

import model.Square;

public class SquareTest {

	@Test
	public void testCalcArea() {
		Square s = new Square();
		s.setBase(4);
		s.setHeight(4);
		s.setArea(s.calcArea());
		Assert.assertEquals(s.getArea(), 16.0, 0.0);
		Assert.assertEquals(s.getBase(), 4, 0.0);
		Assert.assertEquals(s.getHeight(), 4, 0.0);
	}

}
