package test;


import static org.junit.Assert.*;

import org.junit.Test;

import model.Circle;

public class CircleTest {
	
	Circle circletest = new Circle();
	@Test
	public void radtest() {

		circletest.setArea(4);
		circletest.setRadius(1.13);
		circletest.setPerimeter(7.1);
		
		double radius = circletest.getRadius();
		assertEquals(1.13, radius, 0.0);
		
	}
		

}
