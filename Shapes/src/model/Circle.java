package model;
/**
 * 
 */

/**
 * @author Kyle
 * @version 1.0
 */
public class Circle extends Shape {

	/**
	 * radius of the circle
	 */
	private double radius;

	/**
	 * This method returns radius
	 * 
	 * @param radius
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * This method returns the radius
	 * 
	 * @return radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * This method calculates area
	 * 
	 * @return area
	 */
	public double calcArea() {
		double ar = Math.PI * Math.pow(radius, 2);
		return ar;
	}
}
