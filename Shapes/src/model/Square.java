package model;
/**
 * 
 */

/**
 * @author Kyle
 * @version 1.0
 */
public class Square extends Shape {

	/**
	 * the base of the square
	 */
	private double base;
	/**
	 * the height of the square
	 */
	private double height;

	/**
	 * This gets the base
	 * 
	 * @return base
	 */
	public double getBase() {
		return base;
	}

	/**
	 * set the base for the square
	 * 
	 * @param base
	 */
	public void setBase(double base) {
		this.base = base;
	}

	/**
	 * gets the height for the square
	 * 
	 * @return height
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * sets the height for the square
	 * 
	 * @param height
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * calculates the area
	 * 
	 * @return area
	 */
	public double calcArea() {
		double ar = base * height;
		return ar;
	}

}
