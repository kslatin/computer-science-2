package model;

/**
 * @author Kyle
 * @version 1.0
 */

public class Shape {
	private double area;
	private double perimeter;

	/**
	 * This gets the area
	 * 
	 * @return area
	 */
	public double getArea() {
		return area;
	}

	/**
	 * This sets the area
	 * 
	 * @param area
	 */
	public void setArea(double area) {
		this.area = area;
	}

	/**
	 * This returns the perimeter
	 * 
	 * @return perimeter
	 */
	public double getPerimeter() {
		return perimeter;
	}

	public void setPerimeter(double perimeter) {
		this.perimeter = perimeter;
	}

}