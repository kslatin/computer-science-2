package model;

/**
 * 
 */

/**
 * @author Kyle
 * @version 1.0
 */
public class ShapeTest {

	public static void main(String[] args) {

		Circle c = new Circle();
		c.setRadius(6);
		c.setArea(c.calcArea());

		System.out.println("circle radius: " + c.getRadius());
		System.out.println("circle area: " + c.getArea());

		Triangle t = new Triangle();
		t.setSideA(4);
		t.setSideB(5);
		t.setSideC(6);
		t.setArea(t.calcArea());
		System.out.println("sides: " + t.getSideA() + ", " + t.getSideB() + ", " + t.getSideC());
		System.out.println("triangle area: " + t.getArea());

		Square q = new Square();
		q.setBase(4);
		q.setHeight(4);
		q.setArea(q.calcArea());
		System.out.println("Square base: " + q.getBase());
		System.out.println("Square height: " + q.getHeight());
		System.out.println("Square area: " + q.getArea());

	}

}