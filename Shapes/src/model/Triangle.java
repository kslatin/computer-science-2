package model;
/**
 * 
 */

/**
 * @author Kyle
 * @version 1.0
 */
public class Triangle extends Shape {

	/**
	 * Sets the variable for sideA
	 */
	private double sideA;

	/**
	 * Sets the variable for sideB
	 */
	private double sideB;

	/**
	 * Sets the variable for sideC
	 */
	private double sideC;

	/**
	 * This calculates the area.
	 * 
	 * @return area
	 */
	public double calcArea() {
		double semi = ((sideA + sideB + sideC) / 2);
		return (double) Math.sqrt(semi * (semi - sideA) * (semi - sideB) * (semi - sideC));
	}

	/**
	 * This calculates the perimeter
	 * 
	 * @return perimeter
	 */
	public double calcPerimeter() {
		return sideA + sideB + sideC;
	}

	/**
	 * This sets the value of sideA
	 * 
	 * @param sideA
	 */
	public void setSideA(double sideA) {
		this.sideA = sideA;
	}

	/**
	 * This gets the value of sideA
	 * 
	 * @return sideA
	 */
	public double getSideA() {
		return sideA;
	}

	/**
	 * This gets the value of sideB
	 * 
	 * @return sideB
	 */
	public double getSideB() {
		return sideB;
	}

	/**
	 * This sets the value of sideB
	 * 
	 * @param sideB
	 */
	public void setSideB(double sideB) {
		this.sideB = sideB;
	}

	/**
	 * This gets the value of sideC
	 * 
	 * @return sideC
	 */
	public double getSideC() {
		return sideC;
	}

	/**
	 * This sets the value of sideC
	 * 
	 * @param sideC
	 */
	public void setSideC(double sideC) {
		this.sideC = sideC;
	}
}
