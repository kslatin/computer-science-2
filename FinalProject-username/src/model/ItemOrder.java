
package model;

import java.math.BigDecimal;

/**
 * ItemOrder class stores information about a purchase order for a particular
 * item
 * 
 * @author Kyle
 *
 */
public final class ItemOrder {
    private int theQuantity;
    private Item theItem;

    /**
     * Constructor
     * 
     * @param theItem a reference to the item itself
     * @param theQuantity the quantity desired
     */
    public ItemOrder(final Item theItem, final int theQuantity) {
        this.theQuantity = theQuantity;
        this.theItem = theItem;

    }

    /**
     * Calculate order total method
     * 
     * @return the order total
     */
    public BigDecimal calculateOrderTotal() {
        return theItem.calculateItemTotal(theQuantity);
    }

    /**
     * getItem method
     * 
     * @return the reference to the item itself
     */
    public Item getItem() {
        return theItem;
    }

    /**
     * toString method
     * 
     * @return information about the item
     */
    @Override
    public String toString() {
        String result = "";
        result += "Item: " + theItem.toString() + "\n";
        result += "Quantity: " + theQuantity;
        return result;
    }

}
