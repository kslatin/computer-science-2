
package model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ShoppingCart class The ShoppingCart class stores information about the
 * customer's overall purchase
 * 
 * @author Kyle
 *
 */
public class ShoppingCart {
    /**
     * itemOrders is a list of item orders
     */
    private List<ItemOrder> itemOrders;
    /**
     * hasMembership indicates if they have a membership
     */
    private boolean hasMembership;

    /**
     * shoppingCart method creates the list of item orders
     */
    public ShoppingCart() {
        itemOrders = new ArrayList<ItemOrder>();
    }

    /**
     * add method adds the item orders together
     * 
     * @param theOrder
     */
    public void add(final ItemOrder theOrder) {
        // Check for equivalent item
        for (int i = 0; i < itemOrders.size(); i++) {
            if (itemOrders.get(i).getItem().equals(theOrder.getItem())) {
                itemOrders.remove(i);
            }
        }

        itemOrders.add(theOrder);
    }

    /**
     * setMembership sets the membership
     * 
     * @param theMembership
     */
    public void setMembership(final boolean theMembership) {
        this.hasMembership = theMembership;

    }

    /**
     * calculateTotal method calculates the total if it has membership it will
     * reduce the price by 10%
     * 
     * @return total
     */
    public BigDecimal calculateTotal() {
        BigDecimal total = BigDecimal.ZERO;
        for (ItemOrder order : itemOrders) {
            total = total.add(order.calculateOrderTotal());
        }
        if (hasMembership) {
            total = total.multiply(new BigDecimal(90)).divide(new BigDecimal(100));
        }
        return total;
    }

    /**
     * toString method prints the order information and membership information
     */
    @Override
    public String toString() {
        String result = "";
        result += "ItemOrders: " + itemOrders.toString() + "\n";
        result += "Membership: " + hasMembership;
        return result;
    }

    /**
     * Get the item orders <br>
     * Used for testing
     * 
     * @return
     */
    public List<ItemOrder> getItemOrders() {
        return itemOrders;
    }

    /**
     * Get the membership <br>
     * Used for testing
     * 
     * @return
     */
    public boolean isHasMembership() {
        return hasMembership;
    }

}
