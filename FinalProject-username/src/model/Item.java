
package model;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * The Item Class
 * 
 * @author Kyle
 *
 */
public final class Item {

    /**
     * the name of the item
     */
    private String theName;
    /**
     * the price of the item
     */
    private BigDecimal thePrice;
    /**
     * the bulk quantity of the item
     */
    private int theBulkQuantity;
    /**
     * the bulk price of the item
     */
    private BigDecimal theBulkPrice;
    /**
     * number format used for formatting currency
     */
    private static final NumberFormat NF = NumberFormat.getCurrencyInstance();

    /**
     * Constructor
     * 
     * @param theName the name of the item
     * @param thePrice the price of the item
     */
    public Item(final String theName, final BigDecimal thePrice) {

        this.theName = theName;
        this.thePrice = thePrice;
    }

    /**
     * Constructor
     * 
     * @param theName the name of the item
     * @param thePrice the price of the item
     * @param theBulkQuantity the bulk quantity of the item
     * @param theBulkPrice the bulk price of the item
     */
    public Item(final String theName, final BigDecimal thePrice, final int theBulkQuantity,
                final BigDecimal theBulkPrice) {
        this(theName, thePrice);
        this.theBulkQuantity = theBulkQuantity;
        this.theBulkPrice = theBulkPrice;

    }

    /**
     * Calculating Total
     * 
     * @param theQuantity how many of the item
     * @return the total
     */
    public BigDecimal calculateItemTotal(final int theQuantity) {

        BigDecimal total = BigDecimal.ZERO;
        if (hasBulkData()) {
            total = new BigDecimal((theQuantity / theBulkQuantity)).multiply(theBulkPrice);

            if (theQuantity % theBulkQuantity > 0) {
                int leftOver = theQuantity % theBulkQuantity;
                total = total.add(thePrice.multiply(new BigDecimal(leftOver)));

            }
        } else {
            total = new BigDecimal(theQuantity).multiply(thePrice);
        }
        return total;
    }

    /**
     * return true if item has bulk price and bulk quantity
     * 
     * @return boolean whether or not item has bulk price and quantity
     */
    private boolean hasBulkData() {
        return theBulkQuantity > 0 && theBulkPrice.compareTo(BigDecimal.ZERO) > 0;
    }

    /**
     * toString method
     * 
     * @return the string representation of the item
     */
    @Override
    public String toString() {
        String result = "";
        result += theName;
        result += ", ";
        result += NF.format(thePrice);
        if (hasBulkData()) {
            result += " (";
            result += theBulkQuantity;
            result += " for ";
            result += NF.format(theBulkPrice);
            result += ")";
        }
        return result;
    }

    /**
     * @param theOther the other items
     * @return whether or not the other item is equal to this item
     */
    @Override
    public boolean equals(final Object theOther) {
        if (this == theOther) {
            return true;
        }
        if (theOther == null) {
            return false;
        }
        if (theOther instanceof Item == false) {
            return false;
        }

        Item other = (Item) theOther;
        return this.getTheName().equals(other.getTheName())
               && this.getThePrice().compareTo(other.getThePrice()) == 0
               && this.getTheBulkPrice().compareTo(other.getTheBulkPrice()) == 0
               && this.getTheBulkQuantity() == other.getTheBulkQuantity();

    }

    /**
     * item name getter
     * 
     * @return item name
     */
    public String getTheName() {
        return theName;
    }

    /**
     * price getter
     * 
     * @return the price
     */
    public BigDecimal getThePrice() {
        return thePrice;
    }

    /**
     * bulk quantity getter
     * 
     * @return the bulk quantity
     */
    public int getTheBulkQuantity() {
        return theBulkQuantity;
    }

    /**
     * the bulk price getter
     * 
     * @return the bulk price
     */
    public BigDecimal getTheBulkPrice() {
        return theBulkPrice;
    }

}
