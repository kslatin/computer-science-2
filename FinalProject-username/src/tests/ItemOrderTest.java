
package tests;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import model.Item;
import model.ItemOrder;

public class ItemOrderTest {
    Item item = new Item("Bagging", new BigDecimal("400.56"), 10, new BigDecimal("2000.50"));
    Item item2 = new Item("Frodo", new BigDecimal("6.00"));

    // Item orders
    ItemOrder firstOrder = new ItemOrder(item, 2);

    @Test
    public void testCalculateOrderTotal() {
        Assert.assertEquals(new BigDecimal(801.12).doubleValue(),
                            firstOrder.calculateOrderTotal().doubleValue(), 0.0);
    }

    @Test
    public void testGetItem() {
        Assert.assertTrue(item.equals(firstOrder.getItem()));
    }

    @Test
    public void testToString() {
        Assert.assertTrue(firstOrder.toString()
                        .equals("Item: Bagging, $400.56 (10 for $2,000.50)\nQuantity: 2"));
    }

}
