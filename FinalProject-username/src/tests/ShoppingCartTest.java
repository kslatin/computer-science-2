
package tests;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import model.Item;
import model.ItemOrder;
import model.ShoppingCart;

public class ShoppingCartTest {

    Item item = new Item("Bagging", new BigDecimal("400.56"), 10, new BigDecimal("2000.50"));
    Item item2 = new Item("Frodo", new BigDecimal("6.00"));
    ItemOrder firstOrder = new ItemOrder(item, 2);
    ItemOrder secondOrder = new ItemOrder(item2, 3);
    ShoppingCart cart = new ShoppingCart();

    @Test
    public void testAdd() {
        cart.add(firstOrder);
        cart.add(secondOrder);

        Assert.assertEquals(2, cart.getItemOrders().size());

        // Test duplicated order with same item
        ItemOrder dupOrder = new ItemOrder(item, 15);
        cart.add(dupOrder);
        Assert.assertEquals(2, cart.getItemOrders().size());
    }

    @Test
    public void testSetMembership() {
        cart.setMembership(true);
        Assert.assertTrue(cart.isHasMembership());
        cart.setMembership(false);
        Assert.assertFalse(cart.isHasMembership());
    }

    @Test
    public void testCalculateTotal() {
        cart.add(firstOrder);
        cart.add(secondOrder);
        // Test total without membership
        cart.setMembership(false);
        Assert.assertEquals(new BigDecimal(819.12).doubleValue(),
                            cart.calculateTotal().doubleValue(), 0.0);

        // Test total with membership
        cart.setMembership(true);
        Assert.assertEquals(new BigDecimal(737.208).doubleValue(),
                            cart.calculateTotal().doubleValue(), 0.0);

    }

    @Test
    public void testToString() {
        cart.add(firstOrder);
        cart.add(secondOrder);
        System.out.println(cart.toString());
        Assert.assertTrue(cart.toString()
                        .equals("ItemOrders: [Item: Bagging, $400.56 (10 for $2,000.50)\nQuantity: 2, Item: Frodo, $6.00\nQuantity: 3]\nMembership: false"));
    }

}
