
package tests;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import model.Item;
import model.ItemOrder;

public class ItemTest {
    Item item = new Item("Grad Hat", new BigDecimal("4.14"), 8, new BigDecimal("12.48"));
    Item item2 = new Item("Bad Hat", new BigDecimal("6.00"));

    @Test
    public void testCalculateItemTotal() {
        // Test total with bulk price
        assertEquals(new BigDecimal(24.96).doubleValue(),
                     item.calculateItemTotal(16).doubleValue(), 0.0);
        // Total without bulk price
        assertEquals(new BigDecimal(18.00).doubleValue(),
                     item2.calculateItemTotal(3).doubleValue(), 0.0);

        // Total with bulk price and left over
        assertEquals(new BigDecimal(29.1).doubleValue(),
                     item.calculateItemTotal(17).doubleValue(), 0.0);

    }

    @Test
    public void testToString() {
        Assert.assertTrue(item.toString().equals("Grad Hat, $4.14 (8 for $12.48)"));
        Assert.assertTrue(item2.toString().equals("Bad Hat, $6.00"));
        Assert.assertFalse(item.toString().equals("dasjfhsdfgjadsgfkjhsadfsad"));
    }

    @Test
    public void testEqualsObject() {
        // self test
        Assert.assertEquals(item, item);

        // null test
        Assert.assertNotEquals(item, null);

        // Other type test
        ItemOrder order = new ItemOrder(item, 2);
        Assert.assertNotEquals(order, item);

        // Test with identical data
        Item firstItem = new Item("Grad Hat", new BigDecimal("4.14"), 8,
                                  new BigDecimal("12.48"));
        Assert.assertTrue(item.equals(firstItem));

        // item with different name
        firstItem = new Item("Grad Hats", new BigDecimal("4.14"), 8, new BigDecimal("12.48"));
        Assert.assertFalse(item.equals(firstItem));
    }

}
