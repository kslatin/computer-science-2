
package view;

import java.awt.EventQueue;
import java.math.BigDecimal;
import java.util.Arrays;

import model.Item;

/**
 * ShoppingMain provides the main method for a simple shopping cart GUI
 * displayer and calculator.
 * 
 */

public final class ShoppingMain {

    /**
     * An array of items to be displayed in the shopping cart.
     */
    private static final Item[] ITEMS = new Item[] {
                    new Item("Grad Hat", new BigDecimal("4.14"), 8, new BigDecimal("12.48")),
                    new Item("Teddy Bear", new BigDecimal("3.80"), 3, new BigDecimal("10.89")),
                    new Item("Grad Gown", new BigDecimal("429.00")),
                    new Item("Picture Frame", new BigDecimal("6.89")),
                    new Item("Book", new BigDecimal("54.99")),
                    new Item("Chapman Uni Pen", new BigDecimal("1.20")),
                    new Item("Shirt", new BigDecimal("9.99")),
                    new Item("Pants", new BigDecimal("19.01")),
                    new Item("Chapman button", new BigDecimal("0.95"), 10,
                             new BigDecimal("5.00")),
                    new Item("Chapman bumper sticker", new BigDecimal("0.99"), 20,
                             new BigDecimal("8.95"))};

    /**
     * A private constructor, to prevent instances from being constructed.
     */
    private ShoppingMain() {
    }

    /**
     * The main() method - displays and runs the shopping cart GUI.
     * 
     * @param theArgs Command line arguments, ignored by this program.
     */
    public static void main(final String... theArgs) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ShoppingFrame(Arrays.asList(ITEMS));
            }
        });
    } // end main()

} // end class ShoppingMain
